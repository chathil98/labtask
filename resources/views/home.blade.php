@extends('app')
@section('content')
    @foreach ($posts as $post)
        <div class="card mx-auto col-sm-11 d-flex shadow p-3 mb-5 bg-white rounded">
            <div class="card-body">
                <div class="d-flex">
                    <h5 class="card-title mr-auto">{{$post->title}}</h5>
                    <a href="/post/edit/{{$post->id}}" class="btn btn-outline-warning mx-1">Edit</a>
                    <a href="/post/destroy/{{$post->id}}" class="btn btn-outline-danger">Delete</a>
                </div>
                <h6>Hasil Stem : {{(new Sastrawi\Stemmer\StemmerFactory())->createStemmer()->stem($post->title)}}</h6>
                <h6>{{ \Carbon\Carbon::parse($post->created_at)->diffForHumans()}}</h6>
                <p class="card-text">{{$post->content}}</p>
                <a href="#" class="btn btn-primary">More</a>
            </div>
        </div>
    @endforeach
    <div class="align-self-center">{{ $posts->links() }}</div>
@endsection
