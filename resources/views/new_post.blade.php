@extends('app')
@section('content')
    <form action="/post/store" method="post">
        <div class="form-group">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <label for="title">Post Title</label>
            <input type="text" class="form-control" name="title" aria-describedby="postTitle"
                   placeholder="Type the post title">
            <small id="postTitle" class="form-text text-muted">Type your post title here & make sure it's click bait.
            </small>
        </div>
        <div class="form-group">
            <label for="content">Post Content</label>
            <textarea class="form-control" name="content" rows="5"></textarea>
        </div>
        <input type="submit" class="btn btn-primary" value="Submit"/>
    </form>

    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif

@endsection


