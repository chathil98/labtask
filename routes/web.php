<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::resource('post', 'UserController');
Route::get('/', function () {
    return redirect('post');
});

//Route::resource('insert', 'UserController@store');
//Route::resource("posts", 'UserController');
Route::get('post/create', 'UserController@create');
Route::post('post/store', 'UserController@store');
Route::get('post/destroy/{id}', 'UserController@destroy');
Route::get('post/edit/{id}', 'UserController@edit');
Route::put('/post/update/{id}', 'UserController@update');
//Route::get('/form',function() {
//    return view('form');
//});
